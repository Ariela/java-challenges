import java.util.*;

public class practice {
    public static void main(String[] args){
        middleThree("Thenjnjnjnjse");
        String str = "We have a large inventory of things: in our warehouse they are in the category:apperal and the slightly more demand in category:makeup along with the category:furniture and ...";
        printCategories(str);
        reverseString("abcdefg");
        System.out.println(" ");
        getEven("abcdefg");
        System.out.println(" ");
        bigListItem();
        System.out.println(" ");
        search(new int[] {2,3,4,5,6,7}, 5);
        search(new int[] {2,3,4,5,6,7}, 9);
        System.out.println(" ");
        System.out.println(allDollars("hello")); //h$e$l$l$o
    }
//====================================================
    public static String middleThree(String str){
      //  StringBuilder sb = new StringBuilder();
        char[] array = str.toCharArray();
        String result = "";
        if(str.length() >= 3){
            //if string length is greater or equal
            //get mid
            int midIdx = str.length()/2;
            result = str.substring(midIdx - 1, midIdx + 2);
            System.out.println(result);
        }
        return result;
    }
//=====================================================

    public static void printCategories(String str) {
        int start = 0, end = 0;
        while (true) {
            start = (str.indexOf(":", start));
            if (start == -1) {
                break;
            }
            start = start + 1;
            end = (str.indexOf(" ", start));
            System.out.println(str.substring(start, end));
        }
    }

///=================================================
    public static void reverseString(String str){
        /*reverse a string
          loop:
          Initialize to last letter in string
          Test for end of loop
          Decrement each letter in the string
         */
    for(int i = str.length() - 1; i >= 0 ; i--){
            System.out.print(str.charAt(i));
        }
    }
 //=========================================
 public static void getEven(String str){
        /*Two ways to ge even items
           in for loop increment every second position
           if statement use modulo of each character index if no remainer
           its even
         */
     for(int i = 0; i <= str.length(); i+=2){
    // for(int i = 0; i <= str.length(); i++){
      //   if(i % 2 == 0) {
             System.out.print(str.charAt(i));
     //    }
     }
 }
//===========================================
    public static void bigListItem(){
        List<String> list = new ArrayList<>();
        list.add("this is an item in the list");
        list.add("this is a longer item in the list");
        list.add("this is yet another longer item in the list");

        /*Java 8
          Pass in the list and using the Comparator compare each items length
          then get the max length
        * */
      //  String max = Collections.max(list, Comparator.comparing(s -> s.length()));

        String max = Collections.max(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        System.out.println(max);
    }
//============================================================
    public static int search(int [] nums, int target){
        int ret = -1; //if index is not found
        for(int i = 0; i < nums.length; i++){
            if(nums[i] == target){
                ret = i;
                break; //use break if condition is satisfied
            }
        }
        System.out.println(ret);
            return ret;
    }
//============================================================
    /*
    Recursion - looking in mirror with mirror behind you
    you see multiple reflections
    //invoke method in method
    nested structure calls in calls - only change is argument
     */
    public static int printNumber(int num){
        if(num == 0){
            return 0;
        } else {
            System.out.println(num);
        }
        return printNumber(num -1);
    }
    //===============================================
    public static String allDollars(String str){
        StringBuilder sb = new StringBuilder();
        //base case - prevent calling after problem solved
        if(str.length() <= 1){
            return str;
        }
        return str.charAt(0) + "$" +  allDollars(str.substring(1));
    }
}
